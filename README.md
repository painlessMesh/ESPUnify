# ESPUnify

Project to unify the ESP32 and ESP8266 SDK. The goal is to have a thin wrapper around the ESP8266 SDK to make it the same as the ESP32 SDK. This will make it easier for projects to work on both sets of hardware at the same time. We chose to default to the ESP32 design, because that design was informed by mistakes made during the ESP8266 SDK and, therefore, the cleaner, more elegant interface.

# Status

Most basic and some advanced functions are implemented. The library currently works for us and is not under active development (except for bug fixes), but if you want to add functionality then merge requests will be accepted.

# Usage

Instead of including the ESP9266 and ESP32 specific headers (respectively `user_interface.h` and `esp_wifi.h`) you can include this library (`ESPUnity.h`).
