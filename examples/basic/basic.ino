#include "ESPUnify.h"

#define STA_EXAMPLE_WIFI_SSID "SSID"
#define STA_EXAMPLE_WIFI_PASS "87654321"

void setup() {
    Serial.begin(115200);
    Serial.println("");
    Serial.print(micros());
    Serial.println(" Start");

    tcpip_adapter_init();

    //esp_event_loop_init(event_handler, NULL);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&cfg);

    esp_wifi_set_mode(WIFI_MODE_STA);

    Serial.print(micros());
    Serial.println(" Starting wifi");
    esp_wifi_start();
}

// the loop function runs over and over again forever
void loop() {
    delay(1*1000);
}
